<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CustomEventSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private readonly LoggerInterface $logger
    )
    {

    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => [
                ['onPostResponseEvent'],
                ['onPreResponseEvent', 10]
            ]
        ];
    }

    public function onPreResponseEvent(ResponseEvent $event): void
    {
        $this->logger->info(__METHOD__);
    }

    public function onPostResponseEvent(ResponseEvent $event): void
    {
        $this->logger->info(__METHOD__);
    }
}